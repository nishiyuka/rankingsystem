<h1>2022年開花日の早かった上位5位の都市</h1>
<table>
    <tr>
        <th>順位</th>
        <th>都市名</th>
        <th>開花日</th>
    </tr>
    <?php foreach($data as $item): ?>
    <tr>
        <td><?= $item['rank'] ?></td>
        <td><?= $item['name'] ?></td>
        <td><?= $item['date'] ?></td>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<br>
<?= $this->Form->create(null,['url' => ['action' => 'index'],'type' => 'post']) ?>
<?= $this->Form->submit('戻る') ?>
<?= $this->Form->end() ?>
