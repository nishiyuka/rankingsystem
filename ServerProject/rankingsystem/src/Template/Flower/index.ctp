<h1>2022年桜の開花日</h1>
<table>
    <tr>       
        <th>都市名</th>
        <th>開花日</th>
    </tr>
    <?php foreach ($flower as $item): ?>
    <tr>   
        <td><?= $item->name ?></td>
        <td><?= $item->date->format('m/d') ?></td>
        
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<br>
<a href="<?=$this->Url->build(['controller'=>'Flower','action'=>'result']); ?>">
<p>開花日の最も早かった上位5位の都市</p></a>
