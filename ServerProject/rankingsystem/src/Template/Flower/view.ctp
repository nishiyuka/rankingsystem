<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Flower $flower
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Flower'), ['action' => 'edit', $flower->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Flower'), ['action' => 'delete', $flower->id], ['confirm' => __('Are you sure you want to delete # {0}?', $flower->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Flower'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Flower'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="flower view large-9 medium-8 columns content">
    <h3><?= h($flower->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($flower->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($flower->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($flower->date) ?></td>
        </tr>
    </table>
</div>
