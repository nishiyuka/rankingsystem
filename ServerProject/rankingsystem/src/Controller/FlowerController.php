<?php
namespace App\Controller;

use App\Controller\AppController;

class FlowerController extends AppController
{
    public function index()
    {
        $data = $this->Flower->find('all');
        $this->set('flower',$data);
    }

    public function result()
    {
        $countnum = 5;   //表示する順位
        
        $data = $this->Flower->find('all',['order' => ['Flower.date' => 'ASC']]);
        $arraydata= $data->toArray();
        $date = $arraydata[0]['date'];
        $rank = 1;
        $resultdata = [];
        foreach($arraydata as $obj){
            if ($date != $obj['date']){
                $rank ++;
            }
            if ($rank > $countnum){
                break;
            }
            $date = $obj['date'];
            $resultdata[] = [
                'rank' => $rank,
                'name' => $obj['name'],
                'date' => date('m/d',strtotime($obj['date']))
            ];
        }
        
        $this->set('data',$resultdata);
        
    } 
}
