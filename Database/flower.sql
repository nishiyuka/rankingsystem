-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: localhost:3306
-- 生成日時: 2023 年 3 月 18 日 08:50
-- サーバのバージョン： 5.7.39
-- PHP のバージョン: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `flower`
--

CREATE TABLE `flower` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `flower`
--

INSERT INTO `flower` (`id`, `name`, `date`) VALUES
(1, '東京', '2022-03-20'),
(2, '横浜', '2022-03-21'),
(3, '札幌', '2022-04-23'),
(4, '青森', '2022-04-16'),
(5, '仙台', '2022-04-08'),
(6, '水戸', '2022-03-30'),
(7, '大阪', '2022-03-23'),
(8, '高知', '2022-03-19'),
(9, '福岡', '2022-03-17'),
(10, '鹿児島', '2022-03-20'),
(11, '宮崎', '2022-03-18'),
(12, '佐賀', '2022-03-19'),
(13, '秋田', '2022-04-12'),
(14, '山形', '2022-04-11'),
(15, '新潟', '2022-04-08'),
(16, '金沢', '2022-03-30'),
(17, '長野', '2022-04-09'),
(18, '宇都宮', '2022-03-27'),
(19, '前橋', '2022-03-27'),
(20, '熊谷', '2022-03-24'),
(21, '熊本', '2022-03-20'),
(22, '広島', '2022-03-21'),
(23, '奈良', '2022-03-25'),
(24, '京都', '2022-03-24');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `flower`
--
ALTER TABLE `flower`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルの AUTO_INCREMENT
--

--
-- テーブルの AUTO_INCREMENT `flower`
--
ALTER TABLE `flower`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
